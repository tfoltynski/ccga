﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using AlgorytmGenetycznyCC.CCGA;
using System.Collections.Generic;
using AlgorytmGenetycznyCC;
using System.Drawing;

namespace AlgorytmGenetyczntCC.Tests
{
				[TestClass]
				public class MutationTests
				{
								[TestMethod]
								public void MutationTest()
								{
												var obstacle = new List<Shape>();
												obstacle.Add(new Shape(Shape.Type.Rectangle, new Point(10, 10), new Point(20, 20), Color.Black, 1, false));

												GAConfig.PopulationSize = 2;
												GAConfig.NumberOfSpecies = 3;
												GAConfig.MutationRate = 1;
												GAConfig.Width = 20;
												GAConfig.Height = 20;
												GAConfig.ObstacleList = obstacle;

												Individual first = new Individual();
												first.Position.X = 1;
												first.Position.Y = 7;

												Individual second = new Individual();
												second.Position.X = 0;
												second.Position.Y = 4;

												CoopCoevGA ccga = new CoopCoevGA();

												ccga.population[0].PopulationList.Clear();
												ccga.population[0].PopulationList.Add(first);
												ccga.population[0].PopulationList.Add(second);

												ccga.Mutation();

								}
				}
}
