﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using AlgorytmGenetycznyCC;

using System.Drawing;namespace AlgorytmGenetyczntCC.Tests
{
				[TestClass]
				public class HelpersTests
				{
								private Point A = new Point(10, 10);
								private Point B = new Point(20, 10);

								[TestMethod]
								public void TestNotIntersectingPoints()
								{
												Point C = new Point(9, 9);
												Point D = new Point(9, 20);

												var result = Helpers.GetIntersectionPoint(C, D, A, B);

												Assert.AreEqual(0, result.IntersectingPoint.X);
												Assert.AreEqual(0, result.IntersectingPoint.Y);
												Assert.AreEqual(0, result.Length);
								}

								[TestMethod]
								public void InitialPointsOnLineTest()
								{
												Point C = new Point(12, 10);
												Point D = new Point(18, 14);

												var result = Helpers.GetIntersectionPoint(A, B, C, D);

												Assert.AreEqual(12, result.IntersectingPoint.X);
												Assert.AreEqual(10, result.IntersectingPoint.Y);
												Assert.AreEqual(2, result.Length);
								}

								[TestMethod]
								public void TestIntersectingPoints()
								{
												Point C = new Point(12, 9);
												Point D = new Point(12, 13);

												var result = Helpers.GetIntersectionPoint(A, B, C, D);

												Assert.IsTrue(result.IsIntersecting);
												Assert.AreEqual(12, result.IntersectingPoint.X);
												Assert.AreEqual(10, result.IntersectingPoint.Y);
												Assert.AreEqual(2, result.Length);
								}

								[TestMethod]
								public void PointInsideRectTest()
								{
												Point A = new Point(11, 11);
												Point C = new Point(10, 10);
												Point D = new Point(20, 20);
												Rectangle rect = Helpers.GetRectFromPoints(C, D);
												var result = rect.Contains(A);

												Assert.IsTrue(result);
								}
				}
}
