﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using AlgorytmGenetycznyCC.CCGA;
using System.Collections.Generic;
using AlgorytmGenetycznyCC;
using System.Drawing;

namespace AlgorytmGenetyczntCC.Tests
{
				[TestClass]
				public class CrossoverTests
				{
								[TestMethod]
								public void CrossoverTest()
								{
												var obstacle = new List<Shape>();
												obstacle.Add(new Shape(Shape.Type.Rectangle, new Point(10, 10), new Point(20, 20), Color.Black, 1, false));

												GAConfig.CrossoverRate = 1;
												GAConfig.PopulationSize = 2;
												GAConfig.TournamentSize = 5;
												GAConfig.Width = 20;
												GAConfig.Height = 20;
												GAConfig.ObstacleList = obstacle;

												Individual first = new Individual();
												first.Position.X = 31;
												first.Position.Y = 7;

												Individual second = new Individual();
												second.Position.X = 10;
												second.Position.Y = 4;

												var pop = new List<Population>();
												pop.Add(new Population());
												pop[0].PopulationList = new List<Individual> { first, second };

												CoopCoevGA ccga = new CoopCoevGA();
												ccga.population = pop;
												ccga.CalculateFitness(0);
												ccga.TournamentSelection(0);
												ccga.Crossover();

												//Assert.AreEqual();
								}
				}
}
