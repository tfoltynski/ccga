﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using AlgorytmGenetycznyCC.CCGA;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using AlgorytmGenetycznyCC;

namespace AlgorytmGenetyczntCC.Tests
{
				[TestClass]
				public class FitnessTests
				{
								private CoopCoevGA _ccga;

								[TestInitialize]
								public void TestInit()
								{
												var obstacle = new List<Shape>();
												obstacle.Add(new Shape(Shape.Type.Rectangle, new Point(10, 10), new Point(20, 20), Color.Black, 1, false));
												obstacle.Add(new Shape(Shape.Type.Rectangle, new Point(10, 30), new Point(20, 40), Color.Black, 1, false));

												GAConfig.PopulationSize = 1;
												GAConfig.Width = 100;
												GAConfig.Height = 100;
												GAConfig.NumberOfSpecies = 3;
												GAConfig.ObstacleList = obstacle;
												GAConfig.EndPoint = new Point(12, 10);

												_ccga = new CoopCoevGA();
								}

								[TestMethod]
								public void TwoObstaclesFitnessTest()
								{
												var individual = new Individual();
												individual.Position = new Point(12, 8);

												var individual2 = new Individual();
												individual2.Position = new Point(12, 22);

												var individual3 = new Individual();
												individual3.Position = new Point(12, 42);

												_ccga.population[0].PopulationList.Clear();
												_ccga.population[1].PopulationList.Clear();
												_ccga.population[2].PopulationList.Clear();
												_ccga.population[0].PopulationList.Add(individual);
												_ccga.population[1].PopulationList.Add(individual2);
												_ccga.population[2].PopulationList.Add(individual3);

												_ccga.CalculateFitness(0);
												var result = _ccga.population;

												Assert.AreEqual(16, result[0].PopulationList[0].Fitness);
								}

								[TestMethod]
								public void TwoObstaclesOverlapingFitnessTest()
								{
												var obstacle = new List<Shape>();
												obstacle.Add(new Shape(Shape.Type.Rectangle, new Point(10, 10), new Point(20, 20), Color.Black, 1, false));
												obstacle.Add(new Shape(Shape.Type.Rectangle, new Point(10, 18), new Point(20, 28), Color.Black, 1, false));

												GAConfig.ObstacleList = obstacle;

												var individual = new Individual();
												individual.Position = new Point(12, 8);

												var individual2 = new Individual();
												individual2.Position = new Point(12, 14);

												var individual3 = new Individual();
												individual3.Position = new Point(12, 30);

												_ccga.population[0].PopulationList.Clear();
												_ccga.population[1].PopulationList.Clear();
												_ccga.population[2].PopulationList.Clear();
												_ccga.population[0].PopulationList.Add(individual);
												_ccga.population[1].PopulationList.Add(individual2);
												_ccga.population[2].PopulationList.Add(individual3);

												_ccga.CalculateFitness(0);
												var result = _ccga.population;

												Assert.AreEqual(2, result[0].PopulationList[0].Fitness);
								}

								[TestMethod]
								public void IndividualOutsideFitnessTest()
								{
												GAConfig.EndPoint = new Point(10, 12);

												var individual = new Individual();
												individual.Position = new Point(10, 10);

												var individual2 = new Individual();
												individual2.Position = new Point(10, 20);

												//var negative = 3;
												//var result = _ccga.SetFitness(individual, individual2, negative);

												//Assert.AreEqual(9, result);
								}

								[TestMethod]
								public void SwapedIndividualOutsideFitnessTest()
								{
												GAConfig.EndPoint = new Point(10, 22);

												var individual = new Individual();
												individual.Position = new Point(10, 20);

												var individual2 = new Individual();
												individual2.Position = new Point(10, 10);

												var negative = 3;

												//var result = _ccga.SetFitness(individual, individual2, negative);

												//Assert.AreEqual(9, result);
								}

								[TestMethod]
								public void IndividualInsideFitnessTest()
								{
												GAConfig.EndPoint = new Point(10, 12);

												var individual = new Individual();
												individual.Position = new Point(10, 10);

												var individual2 = new Individual();
												individual2.Position = new Point(10, 20);

												var negative = 7;

												//var result = _ccga.SetFitness(individual, individual2, negative);

												//Assert.AreEqual(9, result);
								}

								[TestMethod]
								public void NegativeLineLengthPointsOutsideRectTest()
								{
												GAConfig.EndPoint = new Point(12, 8);
												Rectangle rect = Helpers.GetRectFromPoints(new Point(10, 10), new Point(20, 20));
												Point p = new Point(12, 22);

												List<IntersectionPoint> intersectingPoints = new List<IntersectionPoint>();
												intersectingPoints = _ccga.checkIfLineIntersectsRect(GAConfig.EndPoint, p, rect);

												var result = from element in intersectingPoints
																																									orderby element.Length
																																									select element;
												Assert.AreEqual(2, result.First().Length);
												Assert.AreEqual(12, result.Last().Length);
												Assert.AreEqual(new Point(12, 20), result.Last().IntersectingPoint);
								}
				}
}
