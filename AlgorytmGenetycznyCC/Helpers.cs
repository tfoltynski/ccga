﻿using System;
using System.Collections.Generic;
using System.Diagnostics.Contracts;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AlgorytmGenetycznyCC
{
				/// <summary>
				/// Klasa zawierająca metody pomocnicze
				/// </summary>
				public static class Helpers
				{
								/// <summary>
								/// Zwraca długość lini na podstawie dwóch punktów
								/// </summary>
								public static double GetLineLength(Point p1, Point p2)
								{
												return Math.Sqrt(Math.Pow((p2.X - p1.X), 2) + Math.Pow((p2.Y - p1.Y), 2));
								}

								/// <summary>
								/// Zwraca punkt przecięcia na podstawie dwóch odcinków i przypisuje długość odcinka od punktu początkowego do punktu przecięcia
								/// jeśli nie ma przecięcia zwraca obiekt IntersectionPoint z wartościami domyślnymi
								/// </summary>
								public static IntersectionPoint GetIntersectionPoint(Point A, Point B, Point C, Point D)
								{
												IntersectionPoint ip = new IntersectionPoint();

												int y1 = A.Y - B.Y;
												int x1 = A.X - B.X;
												int y2 = C.Y - D.Y;
												int x2 = C.X - D.X;
												int c1 = A.X * B.Y - A.Y * B.X;
												int c2 = C.X * D.Y - C.Y * D.X;

												float delta = y2 * x1 - y1 * x2;

												if(delta == 0)
												{
																return ip;
												}

												int X = (int)Math.Round((x2 * c1 - x1 * c2) / delta);
												int Y = (int)Math.Round((y2 * c1 - y1 * c2) / delta);

												// Sprawdza czy punkt przecięcia zawiera się w dwóch odcinkach
												if ((X < Math.Min(A.X, B.X) || X > Math.Max(A.X, B.X) || Y < Math.Min(A.Y, B.Y) || Y > Math.Max(A.Y, B.Y)))
												{
																return ip;
												}
												if ((X < Math.Min(C.X, D.X) || X > Math.Max(C.X, D.X) || Y < Math.Min(C.Y, D.Y) || Y > Math.Max(C.Y, D.Y)))
												{
																return ip;
												}

												ip.IntersectingPoint = new Point(X, Y);
												ip.Length = Helpers.GetLineLength(A, ip.IntersectingPoint);
												ip.IsIntersecting = true;

												return ip;
								}

								/// <summary>
								/// Zwraca prostokąt stworzony z dwóch punktów
								/// </summary>
								public static Rectangle GetRectFromPoints(Point r1, Point r2)
								{
												int firstPointX = r1.X;
												int firstPointY = r1.Y;

												if (r1.X > r2.X)
																firstPointX = r2.X;

												if (r1.Y > r2.Y)
																firstPointY = r2.Y;

												return new Rectangle(firstPointX, firstPointY, Math.Abs(r1.X - r2.X), Math.Abs(r1.Y - r2.Y));
								}

				}
}
