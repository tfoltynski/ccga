﻿namespace AlgorytmGenetycznyCC
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
												this.generateBtn = new System.Windows.Forms.Button();
												this.label1 = new System.Windows.Forms.Label();
												this.textBox1 = new System.Windows.Forms.TextBox();
												this.fillObstaclesCB = new System.Windows.Forms.CheckBox();
												this.clearBtn = new System.Windows.Forms.Button();
												this.calculateTrackBtn = new System.Windows.Forms.Button();
												this.label2 = new System.Windows.Forms.Label();
												this.populationSizeTxt = new System.Windows.Forms.TextBox();
												this.label3 = new System.Windows.Forms.Label();
												this.crossoverRateTxt = new System.Windows.Forms.TextBox();
												this.label4 = new System.Windows.Forms.Label();
												this.mutationRateTxt = new System.Windows.Forms.TextBox();
												this.maxGenerationsLbl = new System.Windows.Forms.Label();
												this.maxGenerationsTxt = new System.Windows.Forms.TextBox();
												this.listBox1 = new System.Windows.Forms.ListBox();
												this.iteration_hScrollBar = new System.Windows.Forms.HScrollBar();
												this.myPanel1 = new AlgorytmGenetycznyCC.MyPanel();
												this.SuspendLayout();
												// 
												// generateBtn
												// 
												this.generateBtn.Location = new System.Drawing.Point(12, 74);
												this.generateBtn.Name = "generateBtn";
												this.generateBtn.Size = new System.Drawing.Size(122, 23);
												this.generateBtn.TabIndex = 1;
												this.generateBtn.Text = "Generuj";
												this.generateBtn.UseVisualStyleBackColor = true;
												this.generateBtn.Click += new System.EventHandler(this.generateBtn_Click);
												// 
												// label1
												// 
												this.label1.AutoSize = true;
												this.label1.Location = new System.Drawing.Point(12, 12);
												this.label1.Name = "label1";
												this.label1.Size = new System.Drawing.Size(89, 13);
												this.label1.TabIndex = 2;
												this.label1.Text = "Liczba przeszkód";
												// 
												// textBox1
												// 
												this.textBox1.Location = new System.Drawing.Point(12, 28);
												this.textBox1.Name = "textBox1";
												this.textBox1.Size = new System.Drawing.Size(122, 20);
												this.textBox1.TabIndex = 3;
												this.textBox1.Text = "5";
												// 
												// fillObstaclesCB
												// 
												this.fillObstaclesCB.AutoSize = true;
												this.fillObstaclesCB.Location = new System.Drawing.Point(12, 54);
												this.fillObstaclesCB.Name = "fillObstaclesCB";
												this.fillObstaclesCB.Size = new System.Drawing.Size(108, 17);
												this.fillObstaclesCB.TabIndex = 5;
												this.fillObstaclesCB.Text = "Wypełnij kształty";
												this.fillObstaclesCB.UseVisualStyleBackColor = true;
												this.fillObstaclesCB.CheckedChanged += new System.EventHandler(this.fillObstaclesCB_CheckedChanged);
												// 
												// clearBtn
												// 
												this.clearBtn.Location = new System.Drawing.Point(12, 103);
												this.clearBtn.Name = "clearBtn";
												this.clearBtn.Size = new System.Drawing.Size(122, 23);
												this.clearBtn.TabIndex = 6;
												this.clearBtn.Text = "Wyczyść przeszkody";
												this.clearBtn.UseVisualStyleBackColor = true;
												this.clearBtn.Click += new System.EventHandler(this.clearBtn_Click);
												// 
												// calculateTrackBtn
												// 
												this.calculateTrackBtn.Location = new System.Drawing.Point(12, 394);
												this.calculateTrackBtn.Name = "calculateTrackBtn";
												this.calculateTrackBtn.Size = new System.Drawing.Size(122, 52);
												this.calculateTrackBtn.TabIndex = 7;
												this.calculateTrackBtn.Text = "Start";
												this.calculateTrackBtn.UseVisualStyleBackColor = true;
												this.calculateTrackBtn.Click += new System.EventHandler(this.calculateTrackBtn_Click);
												// 
												// label2
												// 
												this.label2.AutoSize = true;
												this.label2.Location = new System.Drawing.Point(12, 252);
												this.label2.Name = "label2";
												this.label2.Size = new System.Drawing.Size(90, 13);
												this.label2.TabIndex = 9;
												this.label2.Text = "Rozmiar populacji";
												// 
												// populationSizeTxt
												// 
												this.populationSizeTxt.Location = new System.Drawing.Point(12, 268);
												this.populationSizeTxt.Name = "populationSizeTxt";
												this.populationSizeTxt.Size = new System.Drawing.Size(122, 20);
												this.populationSizeTxt.TabIndex = 10;
												this.populationSizeTxt.Text = "100";
												// 
												// label3
												// 
												this.label3.AutoSize = true;
												this.label3.Location = new System.Drawing.Point(9, 291);
												this.label3.Name = "label3";
												this.label3.Size = new System.Drawing.Size(109, 26);
												this.label3.TabIndex = 9;
												this.label3.Text = "Prawdopodobieństwo\r\nkrzyżowania";
												// 
												// crossoverRateTxt
												// 
												this.crossoverRateTxt.Location = new System.Drawing.Point(12, 320);
												this.crossoverRateTxt.Name = "crossoverRateTxt";
												this.crossoverRateTxt.Size = new System.Drawing.Size(122, 20);
												this.crossoverRateTxt.TabIndex = 10;
												this.crossoverRateTxt.Text = "0,6";
												// 
												// label4
												// 
												this.label4.AutoSize = true;
												this.label4.Location = new System.Drawing.Point(9, 343);
												this.label4.Name = "label4";
												this.label4.Size = new System.Drawing.Size(109, 26);
												this.label4.TabIndex = 9;
												this.label4.Text = "Prawdopodobieństwo\r\nmutacji";
												// 
												// mutationRateTxt
												// 
												this.mutationRateTxt.Location = new System.Drawing.Point(12, 372);
												this.mutationRateTxt.Name = "mutationRateTxt";
												this.mutationRateTxt.Size = new System.Drawing.Size(122, 20);
												this.mutationRateTxt.TabIndex = 10;
												this.mutationRateTxt.Text = "0,2";
												// 
												// maxGenerationsLbl
												// 
												this.maxGenerationsLbl.AutoSize = true;
												this.maxGenerationsLbl.Location = new System.Drawing.Point(12, 213);
												this.maxGenerationsLbl.Name = "maxGenerationsLbl";
												this.maxGenerationsLbl.Size = new System.Drawing.Size(84, 13);
												this.maxGenerationsLbl.TabIndex = 9;
												this.maxGenerationsLbl.Text = "Liczba generacji";
												// 
												// maxGenerationsTxt
												// 
												this.maxGenerationsTxt.Location = new System.Drawing.Point(12, 229);
												this.maxGenerationsTxt.Name = "maxGenerationsTxt";
												this.maxGenerationsTxt.Size = new System.Drawing.Size(122, 20);
												this.maxGenerationsTxt.TabIndex = 10;
												this.maxGenerationsTxt.Text = "100";
												// 
												// listBox1
												// 
												this.listBox1.FormattingEnabled = true;
												this.listBox1.HorizontalScrollbar = true;
												this.listBox1.Location = new System.Drawing.Point(12, 128);
												this.listBox1.Name = "listBox1";
												this.listBox1.Size = new System.Drawing.Size(120, 82);
												this.listBox1.TabIndex = 11;
												// 
												// iteration_hScrollBar
												// 
												this.iteration_hScrollBar.Location = new System.Drawing.Point(140, 530);
												this.iteration_hScrollBar.Name = "iteration_hScrollBar";
												this.iteration_hScrollBar.Size = new System.Drawing.Size(510, 24);
												this.iteration_hScrollBar.TabIndex = 12;
												this.iteration_hScrollBar.ValueChanged += new System.EventHandler(this.iteration_hScrollBar_ValueChanged);
												// 
												// myPanel1
												// 
												this.myPanel1.BackColor = System.Drawing.SystemColors.ControlLightLight;
												this.myPanel1.Location = new System.Drawing.Point(140, 12);
												this.myPanel1.Name = "myPanel1";
												this.myPanel1.Size = new System.Drawing.Size(511, 511);
												this.myPanel1.TabIndex = 4;
												this.myPanel1.MouseUp += new System.Windows.Forms.MouseEventHandler(this.myPanel1_MouseUp);
												// 
												// Form1
												// 
												this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
												this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
												this.ClientSize = new System.Drawing.Size(659, 563);
												this.Controls.Add(this.iteration_hScrollBar);
												this.Controls.Add(this.listBox1);
												this.Controls.Add(this.mutationRateTxt);
												this.Controls.Add(this.crossoverRateTxt);
												this.Controls.Add(this.maxGenerationsTxt);
												this.Controls.Add(this.populationSizeTxt);
												this.Controls.Add(this.label4);
												this.Controls.Add(this.maxGenerationsLbl);
												this.Controls.Add(this.label3);
												this.Controls.Add(this.label2);
												this.Controls.Add(this.calculateTrackBtn);
												this.Controls.Add(this.clearBtn);
												this.Controls.Add(this.fillObstaclesCB);
												this.Controls.Add(this.myPanel1);
												this.Controls.Add(this.textBox1);
												this.Controls.Add(this.label1);
												this.Controls.Add(this.generateBtn);
												this.Name = "Form1";
												this.Text = "AlgorytmGenetycznyCC";
												this.ResumeLayout(false);
												this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button generateBtn;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox textBox1;
        private MyPanel myPanel1;
        private System.Windows.Forms.CheckBox fillObstaclesCB;
        private System.Windows.Forms.Button clearBtn;
								private System.Windows.Forms.Button calculateTrackBtn;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox populationSizeTxt;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox crossoverRateTxt;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox mutationRateTxt;
								private System.Windows.Forms.Label maxGenerationsLbl;
								private System.Windows.Forms.TextBox maxGenerationsTxt;
								private System.Windows.Forms.ListBox listBox1;
								private System.Windows.Forms.HScrollBar iteration_hScrollBar;
    }
}

