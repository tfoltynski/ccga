﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace AlgorytmGenetycznyCC
{
    class MyPanel : Panel
    {
        //lista przechowujaca ksztalty na odpowiednich warstwach
        public Dictionary<string, List<Shape>> Layers = new Dictionary<string, List<Shape>>();

        public MyPanel()
        {
            this.SetStyle(
                System.Windows.Forms.ControlStyles.UserPaint |
                System.Windows.Forms.ControlStyles.AllPaintingInWmPaint |
                System.Windows.Forms.ControlStyles.OptimizedDoubleBuffer,
                true);
            this.Layers.Add("obstacles", new List<Shape>());
            this.Layers.Add("startStop", new List<Shape>());
												this.Layers.Add("solution", new List<Shape>());
												this.Layers.Add("gatunek1", new List<Shape>());
												this.Layers.Add("gatunek2", new List<Shape>());
												this.Layers.Add("gatunek3", new List<Shape>());
        }
        
        protected override void OnPaint(PaintEventArgs e)
        {
            base.OnPaint(e);
            e.Graphics.SmoothingMode = SmoothingMode.AntiAlias;
            foreach (var l in Layers)
            {
                foreach(var s in l.Value)
                if (s.type == Shape.Type.Circle)
                {
                    e.Graphics.DrawEllipse(new Pen(s.color, s.grubosc), s.p1.X - s.grubosc, s.p1.Y - s.grubosc, s.grubosc, s.grubosc);
                }
                else if (s.type == Shape.Type.Line)
                {
                    e.Graphics.DrawLine(new Pen(s.color, s.grubosc), s.p1, s.p2);
                }
                else if (s.type == Shape.Type.Rectangle)
																{
																				int firstPointX = s.p1.X;
																				int firstPointY = s.p1.Y;

																				if (s.p1.X > s.p2.X)
																								firstPointX = s.p2.X;

																				if (s.p1.Y > s.p2.Y)
																								firstPointY = s.p2.Y;

																				Rectangle rect = new Rectangle(firstPointX, firstPointY, Math.Abs(s.p1.X - s.p2.X), Math.Abs(s.p1.Y - s.p2.Y));

                    if (s.fill)
                    {
                        e.Graphics.FillRectangle(new SolidBrush(s.color), rect);
                    }
                    else
                    {
                        e.Graphics.DrawRectangle(new Pen(s.color, s.grubosc), rect);
                    }
                }
            }
        }
    }
}
