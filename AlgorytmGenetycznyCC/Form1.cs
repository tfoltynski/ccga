﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using AlgorytmGenetycznyCC.CCGA;

namespace AlgorytmGenetycznyCC
{
    public partial class Form1 : Form
    {
        bool fill = false;
        int step = 1;
        Point p1, p2, startPoint, endPoint;
								CoopCoevGA ccga;
								static Random r = new Random();

        public Form1()
        {
            InitializeComponent();
        }

								/// <summary>
								/// Generuje przeszkody w losowych miejscach
								/// </summary>
        private void generateBtn_Click(object sender, EventArgs e)
        {
												int obstacleNumber = 0;
												try
												{
																obstacleNumber = int.Parse(textBox1.Text);
												}
												catch
												{
																MessageBox.Show("Podano złą wartość!");
												}
												finally
												{
																for (int i = 0; i < obstacleNumber; i++)
																{
																				int firstPosX = r.Next(0, myPanel1.Width - 100);
																				int firstPosY = r.Next(0, myPanel1.Height - 100);

																				p1 = new Point(firstPosX, firstPosY);
																				p2 = new Point(r.Next(firstPosX + 50, firstPosX + 100), r.Next(firstPosY + 50, firstPosY + 100));

																				myPanel1.Layers["obstacles"].Add(new Shape(Shape.Type.Rectangle, p1, p2, Color.Black, 1, fill));
																}
																myPanel1.Invalidate();
												}
        }

								/// <summary>
								/// Wypełnia przeszkody czarnym kolorem
								/// </summary>
        private void fillObstaclesCB_CheckedChanged(object sender, EventArgs e)
        {
												if (fill)
												{
																fill = false;
												}
												else
												{
																fill = true;
												}

												foreach(var o in myPanel1.Layers["obstacles"])
												{
																if (fill)
																{
																				o.fill = true;
																}
																else
																{
																				o.fill = false;
																}
												}
												myPanel1.Invalidate();
        }

								/// <summary>
								/// Usuwa wszystkie przeszkody
								/// </summary>
        private void clearBtn_Click(object sender, EventArgs e)
        {
            myPanel1.Layers["obstacles"].Clear();
            myPanel1.Invalidate();
        }

								/// <summary>
								/// Ustawia punkt początkowy/końcowy po kliknięciu myszką
								/// </summary>
        private void myPanel1_MouseUp(object sender, MouseEventArgs e)
        {
            if (step == 1)
												{
																myPanel1.Layers["solution"].Clear();
																endPoint.X = 0;
																endPoint.Y = 0;
                myPanel1.Layers["startStop"].Clear();
                startPoint = new Point(e.X, e.Y);
                myPanel1.Layers["startStop"].Add(new Shape(Shape.Type.Circle, startPoint, Color.Red, 5));
                step = 2;
            }
            else if (step == 2)
            {
                endPoint = new Point(e.X, e.Y);
                myPanel1.Layers["startStop"].Add(new Shape(Shape.Type.Circle, endPoint, Color.Red, 5));
                step = 1;
            }
            myPanel1.Invalidate();
        }

								/// <summary>
								/// Startuje cały algorytm, ustawiana jest konfiguracja na podstawie wprowadzonych danych
								/// </summary>
        private void calculateTrackBtn_Click(object sender, EventArgs e)
        {
												// Sprawdza czy punkt początkowy i końcowy jest ustawiony
												// w przeciwnym wypadku zwraca informację
												if (startPoint.X != 0 && startPoint.Y != 0 && endPoint.X != 0 && endPoint.Y != 0)
												{
																int populationSize = 0;
																int maxGenerations = 0;
																double crossoverRate = 0;
																double mutationRate = 0;
																bool success = false;

																try
																{
																				populationSize = int.Parse(populationSizeTxt.Text);
																				maxGenerations = int.Parse(maxGenerationsTxt.Text);
																				crossoverRate = double.Parse(crossoverRateTxt.Text);
																				mutationRate = double.Parse(mutationRateTxt.Text);
																				success = true;
																}
																catch (Exception ex)
																{
																				MessageBox.Show(ex.Message);
																}
																finally
																{
																				if (success == true)
																				{
																								myPanel1.Layers["solution"].Clear();
																								listBox1.Items.Clear();
																								iteration_hScrollBar.Maximum = maxGenerations;

																								GAConfig.MaxGenerations = maxGenerations;
																								GAConfig.PopulationSize = populationSize;
																								GAConfig.CrossoverRate = crossoverRate;
																								GAConfig.MutationRate = mutationRate;
																								GAConfig.StartPoint = startPoint;
																								GAConfig.EndPoint = endPoint;
																								GAConfig.Width = myPanel1.Width;
																								GAConfig.Height = myPanel1.Height;
																								GAConfig.NumberOfSpecies = 3;
                        GAConfig.TournamentSize = 2;
																								GAConfig.ObstacleList = myPanel1.Layers["obstacles"];

																								ccga = new CoopCoevGA();

																								// Wywołuje kolejne iteracje
																								for (int i = 0; i < GAConfig.MaxGenerations; i++)
																								{
																												ccga.NextIteration();
																								}

																								iteration_hScrollBar.Value = ccga.currentIteration - 1;
																								drawSolution(iteration_hScrollBar.Value);

																								myPanel1.Invalidate();
																				}
																}
												}
												else
												{
																MessageBox.Show("Nie ustawiono punktu początkowego i/lub końcowego!");
												}
        }

								/// <summary>
								/// Podczas zmiany wartości na pasku pobierane jest rozwiązanie dla wybranej iteracji
								/// </summary>
								private void iteration_hScrollBar_ValueChanged(object sender, EventArgs e)
								{
												myPanel1.Layers["solution"].Clear();
												listBox1.Items.Clear();

												drawSolution(iteration_hScrollBar.Value);

												myPanel1.Invalidate();
								}

								/// <summary>
								/// Rysuje rozwiązanie dla wybranej iteracji
								/// </summary>
								private void drawSolution(int iteration)
								{
												var solution = ccga.GetSolution(iteration);
												var previousPoint = GAConfig.StartPoint;

												foreach (var s in solution)
												{
																myPanel1.Layers["solution"].Add(new Shape(Shape.Type.Circle, s.Position, Color.Red, 3));
																myPanel1.Layers["solution"].Add(new Shape(Shape.Type.Line, previousPoint, s.Position, Color.Red, 1));
																previousPoint = s.Position;
																var sFitness = Math.Round(s.Fitness, 2);
																listBox1.Items.Add(new { sFitness, s.TimesEvaluated, s.Position });
												}

												myPanel1.Layers["solution"].Add(new Shape(Shape.Type.Line, previousPoint, GAConfig.EndPoint, Color.Red, 1));
								}
    }
}
