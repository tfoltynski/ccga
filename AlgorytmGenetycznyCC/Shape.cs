﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AlgorytmGenetycznyCC
{
    public class Shape
    {
        public Type type;
        public Point p1;
        public Point p2;
        public int grubosc;
        public bool fill;
        public Color color { get; set; }

        public enum Type
        {
            Circle,
            Line,
            Rectangle
        }

        /// <summary>
								/// Konstruktor ktory zapisuje ksztalty, pozycje, kolor itd.
        /// </summary>
        public Shape(Type type, Point p1, Point p2, Color color, int grubosc, bool fill)
        {
            this.type = type;
            this.p1 = p1;
            this.p2 = p2;
            this.color = color;
            this.grubosc = grubosc;
            this.fill = fill;
        }

								public Shape(Type type, Point p1, Point p2, Color color, int grubosc)
								{
												this.type = type;
												this.p1 = p1;
												this.p2 = p2;
												this.color = color;
												this.grubosc = grubosc;
								}

        public Shape(Type type, Point p1, Color color, int grubosc)
        {
            this.type = type;
            this.p1 = p1;
            this.color = color;
            this.grubosc = grubosc;
        }
    }
}
