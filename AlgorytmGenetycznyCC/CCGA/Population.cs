﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AlgorytmGenetycznyCC.CCGA
{
				/// <summary>
				/// Klasa reprezentująca całą populację składającą się z osobników
				/// </summary>
				public class Population
				{
								public List<Individual> PopulationList;
								private Individual bestIndividual = new Individual();

								/// <summary>
								/// Tworzy i generuje nową populację
								/// </summary>
								public Population()
								{
												PopulationList = new List<Individual>();
												generatePopulation();
								}

								/// <summary>
								/// Ustawia najlepszego osobnika w populacji
								/// </summary>
								public void SetBestIndividual()
								{
												BestIndividual = PopulationList.OrderBy(x => x.Fitness).First();
								}

								/// <summary>
								/// Generuje losowe osobniki w populacji
								/// </summary>
								private void generatePopulation()
								{
												for (int i = 0; i < GAConfig.PopulationSize; i++)
												{
																PopulationList.Add(new Individual());
												}
								}

								public Individual BestIndividual
								{
												get
												{
																return bestIndividual;
												}
												set
												{
																// Jeśli ocena poprzedniego osobnika jest większa to stary osobnik jest zastępowany nowym z wyższą oceną
																if (bestIndividual.Fitness > value.Fitness)
																				bestIndividual = value.Clone();
												}
								}
				}
}
