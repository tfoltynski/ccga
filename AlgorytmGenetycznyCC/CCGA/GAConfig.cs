﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AlgorytmGenetycznyCC.CCGA
{
				/// <summary>
				/// Klasa statyczna przechowuje konfigurację algorytmu
				/// </summary>
				public static class GAConfig
				{
							 private static int populationSize;
								private static int maxGenerations;
								private static double crossoverRate;
								private static double mutationRate;
        private static int numberOfSpecies;
        private static int tournamentSize;
								private static int width;
								private static int height;
								private static Point startPoint;
								private static Point endPoint;
								private static List<Shape> obstacleList;

								public static int PopulationSize
								{
								    get
												{
																return populationSize;
												}
												set
												{
																populationSize = value;
												}
								}

								public static int MaxGenerations
								{
												get
												{
																return maxGenerations;
												}
												set
												{
																maxGenerations = value;
												}
								}

								public static double CrossoverRate
								{
												get
												{
																return crossoverRate;
												}
												set
												{
																crossoverRate = value;
												}
								}

								public static double MutationRate
								{
												get
												{
																return mutationRate;
												}
												set
												{
																mutationRate = value;
												}
								}

        public static int NumberOfSpecies
        {
            get
            {
                return numberOfSpecies;
            }
            set
            {
                numberOfSpecies = value;
            }
        }

        public static int TournamentSize
        {
            get
            {
                return tournamentSize;
            }
            set
            {
                tournamentSize = value;
            }
        }

								public static int Width
								{
												get
												{
																return width;
												}
												set
												{
																width = value;
												}
								}

								public static int Height
								{
												get
												{
																return height;
												}
												set
												{
																height = value;
												}
								}

								public static Point StartPoint
								{
												get
												{
																return startPoint;
												}
												set
												{
																startPoint = value;
												}
								}

								public static Point EndPoint
								{
												get
												{
																return endPoint;
												}
												set
												{
																endPoint = value;
												}
								}

								public static List<Shape> ObstacleList
								{
												get
												{
																return obstacleList;
												}
												set
												{
																obstacleList = value;
												}
								}
				}
}
