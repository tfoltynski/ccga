﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing;
using System.Threading.Tasks;
using System.Collections;

namespace AlgorytmGenetycznyCC.CCGA
{
				public class CoopCoevGA
				{
								public List<Population> population = new List<Population>();
								public Dictionary<int, List<Individual>> allIterations = new Dictionary<int, List<Individual>>();
								public int currentIteration = 0;

								private List<Individual> selectedPopulation;
								private List<Individual> childrenPopulation;
								private bool firstFitnessCalculation = true;

								private static Random rand = new Random();

								/// <summary>
								/// Odpowiada za wygenerowanie nowych populacji
								/// </summary>
								public CoopCoevGA()
								{
												for (int i = 0; i < GAConfig.NumberOfSpecies; i++)
												{
																population.Add(new Population());
												}
								}

								/// <summary>
								/// Wywołuje kolejne iteracje, najpierw oceniana jest każda populacja
								/// następnie selekcja turniejowa w każdej populacji, krzyżowanie, mutacja
								/// stara populacja jest zastępowana nową populacją
								/// </summary>
								public void NextIteration()
								{
												CalculateFitness();
												List<Individual> solution = new List<Individual>();

												for (int j = 0; j < GAConfig.NumberOfSpecies; j++)
												{
																TournamentSelection(j);
																Crossover();
																Mutation();

																population[j].PopulationList = childrenPopulation;
																population[j].SetBestIndividual();
																solution.Add(population[j].BestIndividual);
												}

												allIterations.Add(this.currentIteration, solution);

												currentIteration++;
								}

								/// <summary>
								/// Oblicza ocenę wszystkich osobników we wszystkich gatunkach
								/// </summary>
								public void CalculateFitness()
								{
												Individual firstIndividual = new Individual();
												Individual secondIndividual = new Individual();
												Individual thirdIndividual = new Individual();
												double totalFitness = 0;
												double firstLineLength = 0;
												double secondLineLength = 0;
												double thirdLineLength = 0;
												double fourthLineLength = 0;
												double currentLineLength;
												for (int j = 0; j < GAConfig.NumberOfSpecies; j++)
												{
																for (int i = 0; i < GAConfig.PopulationSize; i++)
																{
																				firstLineLength = 0;
																				secondLineLength = 0;
																				thirdLineLength = 0;
																				fourthLineLength = 0;
																				currentLineLength = 0;

																				// Jeśli jest to pierwsza ocena to osobniki są wybierane losowo
																				// jeśli następna to wybierane są osobniki najlepsze z innych populacji niż ta która jest oceniana
																				if (firstFitnessCalculation == false)
																				{
																								if (j == 0)
																								{
																												firstIndividual = population[0].PopulationList[rand.Next(GAConfig.PopulationSize)];
																												secondIndividual = population[1].BestIndividual;
																												thirdIndividual = population[2].BestIndividual;
																								}
																								else if (j == 1)
																								{
																												firstIndividual = population[0].BestIndividual;
																												secondIndividual = population[1].PopulationList[rand.Next(GAConfig.PopulationSize)];
																												thirdIndividual = population[2].BestIndividual;
																								}
																								else if (j == 2)
																								{
																												firstIndividual = population[0].BestIndividual;
																												secondIndividual = population[1].BestIndividual;
																												thirdIndividual = population[2].PopulationList[rand.Next(GAConfig.PopulationSize)];
																								}
																				}
																				else
																				{
																								if (j == 0)
																								{
																												firstIndividual = population[0].PopulationList[i];
																												secondIndividual = population[1].PopulationList[i];
																												thirdIndividual = population[2].PopulationList[i];
																								}
																								else if (j == 1)
																								{
																												firstIndividual = population[0].PopulationList[i];
																												secondIndividual = population[1].PopulationList[i];
																												thirdIndividual = population[2].PopulationList[i];
																								}
																								else if (j == 2)
																								{
																												firstIndividual = population[0].PopulationList[i];
																												secondIndividual = population[1].PopulationList[i];
																												thirdIndividual = population[2].PopulationList[i];
																								}
																				}

																				// Wszystkie przeszkody są sprawdzane i obliczana jest długość odcinka znajdującej się w przeszkodzie
																				foreach (var obstacle in GAConfig.ObstacleList)
																				{
																								firstLineLength += GetNegativeLineLength(obstacle, firstIndividual, GAConfig.StartPoint);
																								secondLineLength += GetNegativeLineLength(obstacle, firstIndividual, secondIndividual.Position);
																								thirdLineLength += GetNegativeLineLength(obstacle, thirdIndividual, secondIndividual.Position);
																								fourthLineLength += GetNegativeLineLength(obstacle, thirdIndividual, GAConfig.EndPoint);
																				}

																				currentLineLength = Helpers.GetLineLength(firstIndividual.Position, GAConfig.StartPoint);
																				firstLineLength = firstLineLength * 10 + currentLineLength;

																				currentLineLength = Helpers.GetLineLength(firstIndividual.Position, secondIndividual.Position);
																				secondLineLength = secondLineLength * 10 + currentLineLength;

																				currentLineLength = Helpers.GetLineLength(thirdIndividual.Position, secondIndividual.Position);
																				thirdLineLength = thirdLineLength * 10 + currentLineLength;

																				currentLineLength = Helpers.GetLineLength(thirdIndividual.Position, GAConfig.EndPoint);
																				fourthLineLength = fourthLineLength * 10 + currentLineLength;

																				totalFitness = firstLineLength + secondLineLength + thirdLineLength + fourthLineLength;

																				firstIndividual.TimesEvaluated++;
																				firstIndividual.Fitness = totalFitness;
																				secondIndividual.TimesEvaluated++;
																				secondIndividual.Fitness = totalFitness;
																				thirdIndividual.TimesEvaluated++;
																				thirdIndividual.Fitness = totalFitness;
																}
												}
												firstFitnessCalculation = false;
								}

								/// <summary>
								/// Zwraca długość odcinka znajdującego się w przeszkodzie
								/// </summary>
								public double GetNegativeLineLength(Shape obstacle, Individual currentIndividual, Point secondIndividualPosition)
								{
												List<IntersectionPoint> intersectingPoints = new List<IntersectionPoint>();
												Rectangle rect = Helpers.GetRectFromPoints(obstacle.p1, obstacle.p2);
												double currentLineLength = Helpers.GetLineLength(currentIndividual.Position, secondIndividualPosition);
												bool isPointInsideRect = rect.Contains(currentIndividual.Position);
												double negativeLineLength = 0;

												intersectingPoints = checkIfLineIntersectsRect(currentIndividual.Position, secondIndividualPosition, rect);

												// Sprawdza czy są jakieś punkty przecięcia
												if (intersectingPoints.Count != 0)
												{
																// Punkty przeciecia są sortowane
																var sortedIntersectingPoints = from element in intersectingPoints
																																															orderby element.Length
																																															select element;
																if (intersectingPoints.Count == 1)
																{
																				// Sprawdza czy punkt znajduje się w przeszkodzie
																				if (isPointInsideRect)
																								negativeLineLength += sortedIntersectingPoints.First().Length + 1000;
																				else
																								negativeLineLength += currentLineLength - sortedIntersectingPoints.First().Length;
																}
																else
																{
																				double intersectingLineLength = Helpers.GetLineLength(sortedIntersectingPoints.First().IntersectingPoint, sortedIntersectingPoints.Last().IntersectingPoint);
																				negativeLineLength += intersectingLineLength;
																}
												}
												return negativeLineLength;
								}

								/// <summary>
								/// Zwraca punkt(y) przeciecia na podstawie odcinka i prostokąta
								/// każda krawędz prostokątu jest sprawdzana osobno
								/// </summary>
								public List<IntersectionPoint> checkIfLineIntersectsRect(Point p1, Point p2, Rectangle r)
								{
												IntersectionPoint tempPoint;
												List<IntersectionPoint> intersectingPoints = new List<IntersectionPoint>();

												// Sprawdza czy GÓRNA krawędź prostokątu przecina linia
												tempPoint = Helpers.GetIntersectionPoint(p1, p2, new Point(r.X, r.Y), new Point(r.X + r.Width, r.Y));
												if (tempPoint.IsIntersecting == true)
																intersectingPoints.Add(tempPoint);

												// Sprawdza czy PRAWA krawędź prostokątu przecina linia
												tempPoint = Helpers.GetIntersectionPoint(p1, p2, new Point(r.X + r.Width, r.Y), new Point(r.X + r.Width, r.Y + r.Height));
												if (tempPoint.IsIntersecting == true)
																intersectingPoints.Add(tempPoint);

												// Sprawdza czy DOLNA krawędź prostokątu przecina linia
												tempPoint = Helpers.GetIntersectionPoint(p1, p2, new Point(r.X + r.Width, r.Y + r.Height), new Point(r.X, r.Y + r.Height));
												if (tempPoint.IsIntersecting == true)
																intersectingPoints.Add(tempPoint);

												// Sprawdza czy LEWA krawędź prostokątu przecina linia
												tempPoint = Helpers.GetIntersectionPoint(p1, p2, new Point(r.X, r.Y + r.Height), new Point(r.X, r.Y));
												if (tempPoint.IsIntersecting == true)
																intersectingPoints.Add(tempPoint);

												return intersectingPoints;
								}

								/// <summary>
								/// Wykonuje krzyżowanie dwóch osobników z ustawionym prawdopodobieństwem
								/// zamienia wartości na pozycji X, jeśli osobniki zostały poddane krzyżowaniu
								/// to usuwana jest ich ocena
								/// </summary>
								public void Crossover()
								{
												Individual firstIndividual;
												Individual secondIndividual;
												int firstBinaryTemp;
												int populationSize;
												childrenPopulation = new List<Individual>();

												if (GAConfig.PopulationSize % 2 == 0)
																populationSize = GAConfig.PopulationSize;
												else
																populationSize = GAConfig.PopulationSize - 1;

												for (int i = 0; i < populationSize; i += 2)
												{
																firstIndividual = selectedPopulation[i];
																secondIndividual = selectedPopulation[i + 1];

																if (rand.NextDouble() < GAConfig.CrossoverRate)
																{
																				firstBinaryTemp = firstIndividual.Position.X;

																				firstIndividual.Position.X = secondIndividual.Position.X;
																				secondIndividual.Position.X = firstBinaryTemp;

																				firstIndividual.ClearFitness();
																				secondIndividual.ClearFitness();
																}

																childrenPopulation.Add(firstIndividual.Clone());
																childrenPopulation.Add(secondIndividual.Clone());
												}
												// W przypdaku gdy populacja jest nieparzysta to dodawany jest ostatni osobnik bez krzyżowania do nowej populacji
												if (GAConfig.PopulationSize % 2 == 1)
																childrenPopulation.Add((selectedPopulation[GAConfig.PopulationSize - 1]).Clone());
								}

								/// <summary>
								/// Wykonywana jest mutacja z określonym prawdopodobieństwem
								/// jeśli nastąpi to osobnik przesuwany jest o 1 wartość
								/// tak jak w przypadku krzyżowania osobniki poddane mutacji mają usuwaną ocenę
								/// </summary>
								public void Mutation()
								{
												foreach (Individual individual in childrenPopulation)
												{
																if (rand.NextDouble() < GAConfig.MutationRate)
																{
																				if (rand.NextDouble() < 0.5)
																				{
																								individual.Position.X += 1;
																				}
																				else
																				{
																								individual.Position.X -= 1;
																				}
																				individual.ClearFitness();
																}

																if (rand.NextDouble() < GAConfig.MutationRate)
																{
																				if (rand.NextDouble() < 0.5)
																				{
																								individual.Position.Y += 1;
																				}
																				else
																				{
																								individual.Position.Y -= 1;
																				}
																				individual.ClearFitness();
																}
												}
								}

								/// <summary>
								/// Selekcja turniejowa: losowo są wybierane dwa osobniki
								/// następnie wybierany jest jeden najlepszy z nich
								/// ilość powtórzeń zależy od ilości osobników w populacji
								/// </summary>
								public void TournamentSelection(int populationIndex)
								{
												List<Individual> temp;
												selectedPopulation = new List<Individual>();

												for (int j = 0; j < GAConfig.PopulationSize; j++)
												{
																temp = new List<Individual>();
																for (int i = 0; i < GAConfig.TournamentSize; i++)
																{
																				temp.Add(population[populationIndex].PopulationList[rand.Next(0, GAConfig.PopulationSize)]);
																}
																selectedPopulation.Add(temp.OrderBy(x => x.Fitness).First().Clone());
												}
								}

								/// <summary>
								/// Zwraca rozwiązanie dla podanej iteracji
								/// </summary>
								public List<Individual> GetSolution(int iteration)
								{
												List<Individual> tempSolution = new List<Individual>();
												tempSolution.Add(allIterations[iteration][0]);
												tempSolution.Add(allIterations[iteration][1]);
												tempSolution.Add(allIterations[iteration][2]);

												return tempSolution;
								}
				}
}
