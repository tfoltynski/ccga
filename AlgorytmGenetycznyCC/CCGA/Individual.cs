﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AlgorytmGenetycznyCC.CCGA
{
				/// <summary>
				/// Klasa reprezetntuje jednego osobnika w populacji
				/// </summary>
				public class Individual
				{
								public Point Position;
								public int TimesEvaluated;

								private double _overallFitness;
								private double _fitness = 0;
								static Random rand = new Random();

								/// <summary>
								/// Tworzy nowego osobnika o losowej pozycji
								/// </summary>
								public Individual()
								{
												Position = new Point(rand.Next(GAConfig.Width), rand.Next(GAConfig.Height));
												_overallFitness = 0;
												_fitness = 100000;
												TimesEvaluated = 0;
								}

								/// <summary>
								/// Wewnętrzny konstruktor słóży wyłącznie do kopiowania osobników
								/// </summary>
								internal Individual(double fitness, double overallFitness)
								{
												this._fitness = fitness;
												this._overallFitness = overallFitness;
								}

								/// <summary>
								/// Kopiuje osobnika
								/// </summary>
								public Individual Clone()
								{
												Individual individual = new Individual(this._fitness, this._overallFitness);
												individual.Position = this.Position;
												individual.TimesEvaluated = this.TimesEvaluated;

												return individual;
								}

								/// <summary>
								/// Całkowicie usuwa ocenę osobnika
								/// </summary>
								public void ClearFitness()
								{
												_overallFitness = 0;
												_fitness = 100000;
												TimesEvaluated = 0;
								}

								public double Fitness
								{
												get
												{
																return _fitness;
												}
												set
												{
																_overallFitness += value;
																_fitness = _overallFitness / TimesEvaluated;
												}
								}
				}
}
